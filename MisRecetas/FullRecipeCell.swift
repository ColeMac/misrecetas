//
//  FullRecipeCell.swift
//  MisRecetas
//
//  Created by Cole on 6/15/17.
//  Copyright © 2017 Cole. All rights reserved.
//

import UIKit

class FullRecipeCell: UITableViewCell {

    @IBOutlet var recipeImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
}
