//
//  RecipeCell.swift
//  MisRecetas
//
//  Created by Cole on 6/15/17.
//  Copyright © 2017 Cole. All rights reserved.
//


import UIKit

class RecipeCell: UITableViewCell {
    @IBOutlet var thumbnailImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var ingredientsLabel: UILabel!
    
}
