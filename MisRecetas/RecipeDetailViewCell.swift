//
//  RecipeDetailViewCell.swift
//  MisRecetas
//
//  Created by Cole on 6/22/17.
//  Copyright © 2017 Cole. All rights reserved.
//

import UIKit

class RecipeDetailViewCell: UITableViewCell {
    
    @IBOutlet var keyLabel: UILabel!
    
    @IBOutlet var valueLabel: UILabel!
}
